<?php
/**
 * Created by PhpStorm.
 * User: Jonathan
 * Date: 5/27/2015
 * Time: 9:50 AM
 */

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="navbar-wrapper">
    <div class="container">
        <?php include('navbar.php');?>
    </div>
</div>
<div class="col-md-offset-3 col-md-6 col-xs-offset-1 col-xs-10">
    <form class="form-horizontal">
        <fieldset>
            <legend style="text-align: center; font-size: 40px;">Registro para Usuarios </legend>
            <p>A continuacion ingrese su informacion para registrarse.</p>
            <div class="form-group">
                <label for="inputNombre" class="col-lg-2 control-label">Nombre</label>
                <div class="col-lg-10">
                    <input class="form-control" id="inputNombre" placeholder="Nombre" type="text">
                </div>
            </div>
            <div class="form-group">
                <label for="inputApellidoPat" class="col-lg-2 control-label">Apellido Paterno</label>
                <div class="col-lg-10">
                    <input class="form-control" id="inputApellidoPat" placeholder="Apellido Paterno" type="text">
                </div>
            </div>
            <div class="form-group">
                <label for="inputApellidoMat" class="col-lg-2 control-label">Apellido Materno</label>
                <div class="col-lg-10">
                    <input class="form-control" id="inputApellidoMat" placeholder="Apellido Materno" type="text">
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail" class="col-lg-2 control-label">Email</label>
                <div class="col-lg-10">
                    <input class="form-control" id="inputEmail" placeholder="Email" type="text">
                </div>
            </div>
            <div class="form-group">
                <label for="inputUsuario" class="col-lg-2 control-label">Usuario</label>
                <div class="col-lg-10">
                    <input class="form-control" id="inputUsuario" placeholder="Usario" type="text">
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword" class="col-lg-2 control-label">Password</label>
                <div class="col-lg-10">
                    <input  class="form-control" id="inputPassword" placeholder="Password" type="password">
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword2" class="col-lg-2 control-label">Repita Password</label>
                <div class="col-lg-10">
                    <input class="form-control" id="inputPassword2" placeholder="Password" type="password">
                </div>
            </div>
            <div class="form-group">
                <div class="col-lg-10 col-lg-offset-2">
                    <button type="reset" class="btn btn-default">Cancel</button>
                    <button type="submit" class="btn btn-primary">Registrarse</button>
                </div>
            </div>
        </fieldset>
    </form>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
</body>
</html>