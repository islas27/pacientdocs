<?php
        /**
         * Created by PhpStorm.
         * User: Jonathan
         * Date: 5/26/2015
         * Time: 12:12 AM
         */

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Pagina de Inicio y promocional">
    <meta name="author" content="">
    <title>PacientDocs</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-custom.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<!-- NAVBAR
================================================== -->
    <div class="navbar-wrapper">
        <div class="container">
            <?php include('navbar.php');?>
        </div>
    </div>
    <div class="container">
        <legend style="text-align: center; font-size: 40px;">Registrar Expediente Clinico</legend>
        <form role="form" class="form-horizontal">
        <div class="row">
            <legend>Historia Clinica</legend>
            <div class="col-md-5 col-xs-offset-1">
                <legend>Informacion Personal</legend>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="nombre">Nombre</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="nombre" name="nombre">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="formInput20">Apellido Paterno</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="formInput20" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="formInput20">Apellido Materno</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="formInput20" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="formInput20">Fecha Nac.</label>
                    <div class="col-sm-8">
                        <input type="date" class="form-control" id="formInput20" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="formInput20">Pais Nac.</label>
                    <div class="col-sm-8">
                        <select class="form-control">
                            <option value="Mexico">Mexico</option>
                            <option value="United States" selected="selected">United States</option>
                            <option value="Canada">Canada</option>
                            <option value="United Kingdom" >United Kingdom</option>
                            <option value="Ireland" >Ireland</option>
                            <option value="Australia" >Australia</option>
                            <option value="New Zealand" >New Zealand</option>
                            <option value="null" >-------------------</option>
                            <option value="Afghanistan">Afghanistan</option>
                            <option value="Albania">Albania</option>
                            <option value="Algeria">Algeria</option>
                            <option value="American Samoa">American Samoa</option>
                            <option value="Andorra">Andorra</option>
                            <option value="Angola">Angola</option>
                            <option value="Anguilla">Anguilla</option>
                            <option value="Antarctica">Antarctica</option>
                            <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                            <option value="Argentina">Argentina</option>
                            <option value="Armenia">Armenia</option>
                            <option value="Aruba">Aruba</option>
                            <option value="Australia">Australia</option>
                            <option value="Austria">Austria</option>
                            <option value="Azerbaijan">Azerbaijan</option>
                            <option value="Bahamas">Bahamas</option>
                            <option value="Bahrain">Bahrain</option>
                            <option value="Bangladesh">Bangladesh</option>
                            <option value="Barbados">Barbados</option>
                            <option value="Belarus">Belarus</option>
                            <option value="Belgium">Belgium</option>
                            <option value="Belize">Belize</option>
                            <option value="Benin">Benin</option>
                            <option value="Bermuda">Bermuda</option>
                            <option value="Bhutan">Bhutan</option>
                            <option value="Bolivia">Bolivia</option>
                            <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                            <option value="Botswana">Botswana</option>
                            <option value="Bouvet Island">Bouvet Island</option>
                            <option value="Brazil">Brazil</option>
                            <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                            <option value="Brunei Darussalam">Brunei Darussalam</option>
                            <option value="Bulgaria">Bulgaria</option>
                            <option value="Burkina Faso">Burkina Faso</option>
                            <option value="Burundi">Burundi</option>
                            <option value="Cambodia">Cambodia</option>
                            <option value="Cameroon">Cameroon</option>
                            <option value="Canada">Canada</option>
                            <option value="Cape Verde">Cape Verde</option>
                            <option value="Cayman Islands">Cayman Islands</option>
                            <option value="Central African Republic">Central African Republic</option>
                            <option value="Chad">Chad</option>
                            <option value="Chile">Chile</option>
                            <option value="China">China</option>
                            <option value="Christmas Island">Christmas Island</option>
                            <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
                            <option value="Colombia">Colombia</option>
                            <option value="Comoros">Comoros</option>
                            <option value="Congo">Congo</option>
                            <option value="Congo, The Democratic Republic of The">Congo, The Democratic Republic of The</option>
                            <option value="Cook Islands">Cook Islands</option>
                            <option value="Costa Rica">Costa Rica</option>
                            <option value="Cote D'ivoire">Cote D'ivoire</option>
                            <option value="Croatia">Croatia</option>
                            <option value="Cuba">Cuba</option>
                            <option value="Cyprus">Cyprus</option>
                            <option value="Czech Republic">Czech Republic</option>
                            <option value="Denmark">Denmark</option>
                            <option value="Djibouti">Djibouti</option>
                            <option value="Dominica">Dominica</option>
                            <option value="Dominican Republic">Dominican Republic</option>
                            <option value="Ecuador">Ecuador</option>
                            <option value="Egypt">Egypt</option>
                            <option value="El Salvador">El Salvador</option>
                            <option value="Equatorial Guinea">Equatorial Guinea</option>
                            <option value="Eritrea">Eritrea</option>
                            <option value="Estonia">Estonia</option>
                            <option value="Ethiopia">Ethiopia</option>
                            <option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
                            <option value="Faroe Islands">Faroe Islands</option>
                            <option value="Fiji">Fiji</option>
                            <option value="Finland">Finland</option>
                            <option value="France">France</option>
                            <option value="French Guiana">French Guiana</option>
                            <option value="French Polynesia">French Polynesia</option>
                            <option value="French Southern Territories">French Southern Territories</option>
                            <option value="Gabon">Gabon</option>
                            <option value="Gambia">Gambia</option>
                            <option value="Georgia">Georgia</option>
                            <option value="Germany">Germany</option>
                            <option value="Ghana">Ghana</option>
                            <option value="Gibraltar">Gibraltar</option>
                            <option value="Greece">Greece</option>
                            <option value="Greenland">Greenland</option>
                            <option value="Grenada">Grenada</option>
                            <option value="Guadeloupe">Guadeloupe</option>
                            <option value="Guam">Guam</option>
                            <option value="Guatemala">Guatemala</option>
                            <option value="Guinea">Guinea</option>
                            <option value="Guinea-bissau">Guinea-bissau</option>
                            <option value="Guyana">Guyana</option>
                            <option value="Haiti">Haiti</option>
                            <option value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald Islands</option>
                            <option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option>
                            <option value="Honduras">Honduras</option>
                            <option value="Hong Kong">Hong Kong</option>
                            <option value="Hungary">Hungary</option>
                            <option value="Iceland">Iceland</option>
                            <option value="India">India</option>
                            <option value="Indonesia">Indonesia</option>
                            <option value="Iran, Islamic Republic of">Iran, Islamic Republic of</option>
                            <option value="Iraq">Iraq</option>
                            <option value="Ireland">Ireland</option>
                            <option value="Israel">Israel</option>
                            <option value="Italy">Italy</option>
                            <option value="Jamaica">Jamaica</option>
                            <option value="Japan">Japan</option>
                            <option value="Jordan">Jordan</option>
                            <option value="Kazakhstan">Kazakhstan</option>
                            <option value="Kenya">Kenya</option>
                            <option value="Kiribati">Kiribati</option>
                            <option value="Korea, Democratic People's Republic of">Korea, Democratic People's Republic of</option>
                            <option value="Korea, Republic of">Korea, Republic of</option>
                            <option value="Kuwait">Kuwait</option>
                            <option value="Kyrgyzstan">Kyrgyzstan</option>
                            <option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option>
                            <option value="Latvia">Latvia</option>
                            <option value="Lebanon">Lebanon</option>
                            <option value="Lesotho">Lesotho</option>
                            <option value="Liberia">Liberia</option>
                            <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
                            <option value="Liechtenstein">Liechtenstein</option>
                            <option value="Lithuania">Lithuania</option>
                            <option value="Luxembourg">Luxembourg</option>
                            <option value="Macao">Macao</option>
                            <option value="Macedonia, The Former Yugoslav Republic of">Macedonia, The Former Yugoslav Republic of</option>
                            <option value="Madagascar">Madagascar</option>
                            <option value="Malawi">Malawi</option>
                            <option value="Malaysia">Malaysia</option>
                            <option value="Maldives">Maldives</option>
                            <option value="Mali">Mali</option>
                            <option value="Malta">Malta</option>
                            <option value="Marshall Islands">Marshall Islands</option>
                            <option value="Martinique">Martinique</option>
                            <option value="Mauritania">Mauritania</option>
                            <option value="Mauritius">Mauritius</option>
                            <option value="Mayotte">Mayotte</option>
                            <option value="Micronesia, Federated States of">Micronesia, Federated States of</option>
                            <option value="Moldova, Republic of">Moldova, Republic of</option>
                            <option value="Monaco">Monaco</option>
                            <option value="Mongolia">Mongolia</option>
                            <option value="Montserrat">Montserrat</option>
                            <option value="Morocco">Morocco</option>
                            <option value="Mozambique">Mozambique</option>
                            <option value="Myanmar">Myanmar</option>
                            <option value="Namibia">Namibia</option>
                            <option value="Nauru">Nauru</option>
                            <option value="Nepal">Nepal</option>
                            <option value="Netherlands">Netherlands</option>
                            <option value="Netherlands Antilles">Netherlands Antilles</option>
                            <option value="New Caledonia">New Caledonia</option>
                            <option value="New Zealand">New Zealand</option>
                            <option value="Nicaragua">Nicaragua</option>
                            <option value="Niger">Niger</option>
                            <option value="Nigeria">Nigeria</option>
                            <option value="Niue">Niue</option>
                            <option value="Norfolk Island">Norfolk Island</option>
                            <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                            <option value="Norway">Norway</option>
                            <option value="Oman">Oman</option>
                            <option value="Pakistan">Pakistan</option>
                            <option value="Palau">Palau</option>
                            <option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option>
                            <option value="Panama">Panama</option>
                            <option value="Papua New Guinea">Papua New Guinea</option>
                            <option value="Paraguay">Paraguay</option>
                            <option value="Peru">Peru</option>
                            <option value="Philippines">Philippines</option>
                            <option value="Pitcairn">Pitcairn</option>
                            <option value="Poland">Poland</option>
                            <option value="Portugal">Portugal</option>
                            <option value="Puerto Rico">Puerto Rico</option>
                            <option value="Qatar">Qatar</option>
                            <option value="Reunion">Reunion</option>
                            <option value="Romania">Romania</option>
                            <option value="Russian Federation">Russian Federation</option>
                            <option value="Rwanda">Rwanda</option>
                            <option value="Saint Helena">Saint Helena</option>
                            <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                            <option value="Saint Lucia">Saint Lucia</option>
                            <option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
                            <option value="Saint Vincent and The Grenadines">Saint Vincent and The Grenadines</option>
                            <option value="Samoa">Samoa</option>
                            <option value="San Marino">San Marino</option>
                            <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                            <option value="Saudi Arabia">Saudi Arabia</option>
                            <option value="Senegal">Senegal</option>
                            <option value="Serbia and Montenegro">Serbia and Montenegro</option>
                            <option value="Seychelles">Seychelles</option>
                            <option value="Sierra Leone">Sierra Leone</option>
                            <option value="Singapore">Singapore</option>
                            <option value="Slovakia">Slovakia</option>
                            <option value="Slovenia">Slovenia</option>
                            <option value="Solomon Islands">Solomon Islands</option>
                            <option value="Somalia">Somalia</option>
                            <option value="South Africa">South Africa</option>
                            <option value="South Georgia and The South Sandwich Islands">South Georgia and The South Sandwich Islands</option>
                            <option value="Spain">Spain</option>
                            <option value="Sri Lanka">Sri Lanka</option>
                            <option value="Sudan">Sudan</option>
                            <option value="Suriname">Suriname</option>
                            <option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
                            <option value="Swaziland">Swaziland</option>
                            <option value="Sweden">Sweden</option>
                            <option value="Switzerland">Switzerland</option>
                            <option value="Syrian Arab Republic">Syrian Arab Republic</option>
                            <option value="Taiwan, Province of China">Taiwan, Province of China</option>
                            <option value="Tajikistan">Tajikistan</option>
                            <option value="Tanzania, United Republic of">Tanzania, United Republic of</option>
                            <option value="Thailand">Thailand</option>
                            <option value="Timor-leste">Timor-leste</option>
                            <option value="Togo">Togo</option>
                            <option value="Tokelau">Tokelau</option>
                            <option value="Tonga">Tonga</option>
                            <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                            <option value="Tunisia">Tunisia</option>
                            <option value="Turkey">Turkey</option>
                            <option value="Turkmenistan">Turkmenistan</option>
                            <option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
                            <option value="Tuvalu">Tuvalu</option>
                            <option value="Uganda">Uganda</option>
                            <option value="Ukraine">Ukraine</option>
                            <option value="United Arab Emirates">United Arab Emirates</option>
                            <option value="United Kingdom">United Kingdom</option>
                            <option value="United States">United States</option>
                            <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                            <option value="Uruguay">Uruguay</option>
                            <option value="Uzbekistan">Uzbekistan</option>
                            <option value="Vanuatu">Vanuatu</option>
                            <option value="Venezuela">Venezuela</option>
                            <option value="Viet Nam">Viet Nam</option>
                            <option value="Virgin Islands, British">Virgin Islands, British</option>
                            <option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option>
                            <option value="Wallis and Futuna">Wallis and Futuna</option>
                            <option value="Western Sahara">Western Sahara</option>
                            <option value="Yemen">Yemen</option>
                            <option value="Zambia">Zambia</option>
                            <option value="Zimbabwe">Zimbabwe</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="formInput20">Estado Nac.</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="formInput20" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="formInput50">Ciudad Nac.</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="formInput50" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="formInput56">Edad</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="formInput56" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label             col-sm-4" for="formInput32">Sexo</label>
                    <div class="col-sm-8">
                        <select id="formInput32" class="form-control">
                            <option>M</option>
                            <option>F</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label            col-sm-4" for="formInput40">Edo. Civil</label>
                    <div class="col-sm-8">
                        <select id="formInput40" class="form-control">
                            <option>Soltero(a)</option>
                            <option>Casado(a)</option>
                            <option>Divorciado(a)</option>
                            <option>Union Libre</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="formInput20">Ocupacion</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="formInput20" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="formInput20">Domicilio</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="formInput20" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="formInput20">Colonia</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="formInput20" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="formInput20">Telefono</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="formInput20" >
                    </div>
                </div>
                <legend>Familiar Responsable</legend>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="formInput20">Nombre</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="formInput20" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="formInput20">Domicilio</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="formInput20" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="exampleInputPassword1">Telefono</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="exampleInputPassword1">
                    </div>
                </div>
            </div>
            <div class="col-md-5 col-xs-offset-0">
                <legend>Historia Medica</legend>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="formInput60">Motivo de la consulta</label>
                    <div class="col-sm-8">
                        <textarea class="form-control" rows="3" id="formInput60"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="formInput64">Antecedentes Familiares</label>
                    <div class="col-sm-8">
                        <textarea class="form-control" rows="3" id="formInput64"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="formInput72">Higiene General</label>
                    <div class="col-sm-8">
                        <select id="formInput72" class="form-control">
                            <option>Excelente</option>
                            <option>Buena</option>
                            <option>Regular</option>
                            <option>Mala</option>
                            <option>Pesima</option>
                            <option>Nula</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="checkbox">
                            <label class="control-label">
                                <input type="checkbox" value="">&nbsp;&nbsp;Embarazo
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="formInput76">Trimestre</label>
                    <div class="col-sm-6">
                        <select id="formInput76" class="form-control">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                        </select>
                    </div>
                </div>
                <legend>Inmunizaciones</legend>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="checkbox">
                            <label class="control-label">
                                <input type="checkbox" value="">&nbsp; &nbsp; BCG
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="checkbox">
                            <label class="control-label">
                                <input type="checkbox" value="">&nbsp; &nbsp; DPTT
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="checkbox">
                            <label class="control-label">
                                <input type="checkbox" value="">&nbsp; &nbsp; SABIN
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="checkbox">
                            <label class="control-label">
                                <input type="checkbox" value="">&nbsp; &nbsp; ANTISARAMPION
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="checkbox">
                            <label class="control-label">
                                <input type="checkbox" value="">&nbsp; &nbsp; HEPATITIS
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="formInput20">Otras</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="formInput20" >
                    </div>
                </div>
                <legend>Vicios</legend>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="checkbox">
                            <label class="control-label">
                                <input type="checkbox" value="">&nbsp; &nbsp; Tabaquismo
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="checkbox">
                            <label class="control-label">
                                <input type="checkbox" value="">&nbsp; &nbsp; Alcoholismo
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="checkbox">
                            <label class="control-label">
                                <input type="checkbox" value="">&nbsp; &nbsp; Farmacodependiente
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <legend>Antecendetes Patologicos</legend>
            <div class="col-xs-offset-1 col-xs-10 col-md-offset-1 col-md-5 col-lg-offset-0 col-lg-4">
                <div class="form-group antecedentes">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="checkbox">
                            <label class="control-label">
                                <input type="checkbox" value="">&nbsp; &nbsp; Asma
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group antecedentes">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="checkbox">
                            <label class="control-label">
                                <input type="checkbox" value="">&nbsp; &nbsp; Fiebre de Heno
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group antecedentes">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="checkbox">
                            <label class="control-label">
                                <input type="checkbox" value="">&nbsp; &nbsp; Tuberculosis
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group antecedentes">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="checkbox">
                            <label class="control-label">
                                <input type="checkbox" value="">&nbsp; &nbsp; Fibre Reumatica
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group antecedentes">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="checkbox">
                            <label class="control-label">
                                <input type="checkbox" value="">&nbsp; &nbsp; Fiebre Escarlata
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group antecedentes">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="checkbox">
                            <label class="control-label">
                                <input type="checkbox" value="">&nbsp; &nbsp; Soplo en el Corazon
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group antecedentes">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="checkbox">
                            <label class="control-label">
                                <input type="checkbox" value="">&nbsp; &nbsp; Enfermedad Cardiaca
                            </label>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-xs-offset-1 col-xs-10 col-md-offset-1 col-md-5 col-lg-offset-0 col-lg-4">
                <div class="form-group antecedentes">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="checkbox">
                            <label class="control-label">
                                <input type="checkbox" value="">&nbsp; &nbsp; Infarto de Corazon
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group antecedentes">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="checkbox">
                            <label class="control-label">
                                <input type="checkbox" value="">&nbsp; &nbsp; Dificultad de subir escaleras
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group antecedentes">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="checkbox">
                            <label class="control-label">
                                <input type="checkbox" value="">&nbsp; &nbsp; Angina de Pecho
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group antecedentes">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="checkbox">
                            <label class="control-label">
                                <input type="checkbox" value="">&nbsp; &nbsp; Paralisis
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group antecedentes">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="checkbox">
                            <label class="control-label">
                                <input type="checkbox" value="">&nbsp; &nbsp; Presion Alta
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group antecedentes">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="checkbox">
                            <label class="control-label">
                                <input type="checkbox" value="">&nbsp; &nbsp; Presion Baja
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group antecedentes">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="checkbox">
                            <label class="control-label">
                                <input type="checkbox" value="">&nbsp; &nbsp; Anemia
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-offset-1 col-xs-10 col-md-offset-1 col-md-5 col-lg-offset-0 col-lg-4">
                <div class="form-group antecedentes">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="checkbox">
                            <label class="control-label">
                                <input type="checkbox" value="">&nbsp; &nbsp; Alergias
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group antecedentes">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="checkbox">
                            <label class="control-label">
                                <input type="checkbox" value="">&nbsp; &nbsp; Ulceras
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group antecedentes">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="checkbox">
                            <label class="control-label">
                                <input type="checkbox" value="">&nbsp; &nbsp; Artritis
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group antecedentes">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="checkbox">
                            <label class="control-label">
                                <input type="checkbox" value="">&nbsp; &nbsp; Hemorragias Frecuentes
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group antecedentes">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="checkbox">
                            <label class="control-label">
                                <input type="checkbox" value="">&nbsp; &nbsp; Sangrado al Toser
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group antecedentes">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="checkbox">
                            <label class="control-label">
                                <input type="checkbox" value="">&nbsp; &nbsp; Manchas en Piel
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group antecedentes">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="checkbox">
                            <label class="control-label">
                                <input type="checkbox" value="">&nbsp; &nbsp; Moretones Facilmente
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-offset-1 col-xs-10 col-md-offset-1 col-md-5 col-lg-offset-0 col-lg-4">
                <div class="form-group antecedentes">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="checkbox">
                            <label class="control-label">
                                <input type="checkbox" value="">&nbsp; &nbsp; Reumatismo
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group antecedentes">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="checkbox">
                            <label class="control-label">
                                <input type="checkbox" value="">&nbsp; &nbsp; Cefalea Frequente
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group antecedentes">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="checkbox">
                            <label class="control-label">
                                <input type="checkbox" value="">&nbsp; &nbsp; Enfermedades Venereas
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group antecedentes">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="checkbox">
                            <label class="control-label">
                                <input type="checkbox" value="">&nbsp; &nbsp; Enfermedad de Rinion/Higado
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group antecedentes">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="checkbox">
                            <label class="control-label">
                                <input type="checkbox" value="">&nbsp; &nbsp; Hepatitis
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group antecedentes">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="checkbox">
                            <label class="control-label">
                                <input type="checkbox" value="">&nbsp; &nbsp; Diabetes
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group antecedentes">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="checkbox">
                            <label class="control-label">
                                <input type="checkbox" value="">&nbsp; &nbsp; Tobillos Inflamados
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-offset-1 col-xs-10 col-md-offset-1 col-md-5 col-lg-offset-0 col-lg-4">
                <div class="form-group antecedentes">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="checkbox">
                            <label class="control-label">
                                <input type="checkbox" value="">&nbsp; &nbsp; Nerviosismo
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group antecedentes">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="checkbox">
                            <label class="control-label">
                                <input type="checkbox" value="">&nbsp; &nbsp; Deficiencia vitaminica
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group antecedentes">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="checkbox">
                            <label class="control-label">
                                <input type="checkbox" value="">&nbsp; &nbsp; Desmayos
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group antecedentes">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="checkbox">
                            <label class="control-label">
                                <input type="checkbox" value="">&nbsp; &nbsp; Epilepsia/Convulsiones
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group antecedentes">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="checkbox">
                            <label class="control-label">
                                <input type="checkbox" value="">&nbsp; &nbsp; Cataratas/Glaucoma
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group antecedentes">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="checkbox">
                            <label class="control-label">
                                <input type="checkbox" value="">&nbsp; &nbsp; Coagulacion Lenta
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group antecedentes">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="checkbox">
                            <label class="control-label">
                                <input type="checkbox" value="">&nbsp; &nbsp; Dificultad para tragar
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-offset-1 col-xs-10 col-md-offset-1 col-md-5 col-lg-offset-0 col-lg-4">
                <div class="form-group antecedentes">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="checkbox">
                            <label class="control-label">
                                <input type="checkbox" value="">&nbsp; &nbsp; Enfermedad de la Tiroides
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group antecedentes">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="checkbox">
                            <label class="control-label">
                                <input type="checkbox" value="">&nbsp; &nbsp; Radioterapia
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group antecedentes">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="checkbox">
                            <label class="control-label">
                                <input type="checkbox" value="">&nbsp; &nbsp; Tratamiento Psiquiatrico
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group antecedentes">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="checkbox">
                            <label class="control-label">
                                <input type="checkbox" value="">&nbsp; &nbsp; Seropositivo (VIH)
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group antecedentes">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="checkbox">
                            <label class="control-label">
                                <input type="checkbox" value="">&nbsp; &nbsp; Quimioterapia (Cancer/Leucemia)
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group antecedentes">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="checkbox">
                            <label class="control-label">
                                <input type="checkbox" value="">&nbsp; &nbsp; Problemas en intestino (parasitos)
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group antecedentes">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="checkbox">
                            <label class="control-label">
                                <input type="checkbox" value="">&nbsp; &nbsp; Sigue Alguna dieta
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group">
                <div class="col-xs-10 col-xs-offset-1">
                    <br>
                    <button type="submit" class="col-xs-12 btn btn-primary">Guardar</button>
                </div>
            </div>
        </div>
        </form>
    </div>



    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
</body>
</html>